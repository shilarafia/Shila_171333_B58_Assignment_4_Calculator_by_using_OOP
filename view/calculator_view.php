<?php
require_once("../src/calc.php");
$obj=new Calculator();
?>

<html>
<head>
    <title>Calculator</title>
    <link href="../resource/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <script src="../resource/bootstrap/js/bootstrap.min.js"></script>
    <style>
        .pagination{

            margin: 0;
        }


        .table-striped > tbody > tr:nth-child(odd) > td
        {
            background-color: palevioletred;

        }
        .table-striped > tbody > tr:nth-child(even) > td
        {
            background-color: slateblue;
        }
    </style>
</head>

    <body>
<form action="calculator_view.php" method="post">
    <table class="table table-bordered table-striped" border="4">
        <h1 class="alert-info" align="center">Calculator</h1>

        <tr>
            <td>First Number</td>
            <td>
                <input type="number" name="first_number" value="<?php echo $_POST['first_number'];?>" placeholder="0">
            </td>
        </tr>
        <tr>
            <td>Second Number</td>
            <td>
                <input type="number" name="second_number" value="<?php echo $_POST['second_number'];?>" placeholder="0">

            </td>
        </tr>
        <tr>

            <td>
                <input type="submit" name="btn" value="+">
                <input type="submit" name="btn" value="-">
                <input type="submit" name="btn" value="*">
                <input type="submit" name="btn" value="/">
            </td>
        </tr>

        <?php
        if(isset($_POST['btn']))
        {
            $first_number=$_POST['first_number'];
            $second_number=$_POST['second_number'];

            if($_POST['btn']=='+')
            {
                $result=$obj->add($first_number,$second_number);

            }
            if($_POST['btn']=='-')
            {
                $result=$obj->subtract($first_number,$second_number);

            }
            if($_POST['btn']=='*')
            {
                $result=$obj->multiply($first_number,$second_number);

            }
            if($_POST['btn']=='/')
            {
                $result=$obj->divide($first_number,$second_number);

            }

        }
        ?>
        <tr>
            <td>
            <?php
            echo $result['lbl'];
            ?>
            </td>
            <td>
                <?php
                echo $result['value'];
                ?>
            </td>
        </tr>


    </table>

</form>
</body>

</html>
